#!/usr/bin/python

import sys
import os
import getopt
import getpass

# Help function
def help():
    print 'vnstat.py -h -v -i <interface> -d <duration [d]ays|[m]onths> -w <warning in MiB> -c <critical in MiB>'

# Status print function
def status(type, sended, recieved, limit):
    print '%s - %d MiB sended, %d MiB recieved - %d MiB total - %d MiB limit' % (type, sended, recieved, sended + recieved, limit)

# exit codes
exit = {'OK': 0, 'WARNING': 1, 'CRITICAL': 2, 'UNKNOWN': 3}

# default values
verbose = False
duration = 'm'
interface = 'eth0'
warning = '800'
critical = '1000'

# Parsing arguments and setting variables
try:
    opts, args = getopt.getopt(sys.argv[1:],'hvi:d:w:c:', ['help', 'verbose', 'interface=', 'duration=', 'warning=', 'critical='])
except getopt.GetoptError:
    help()
    sys.exit(exit['Unknown'])

for opt, arg in opts:
    if opt == '-h':
        help()
        sys.exit(exit['OK'])
    elif opt in ('-v', '--verbose'):
        verbose = True
    elif opt in ('-i', '--interface'):
        interface = arg
    elif opt in ('-w', '--warning'):
        warning = int(arg)
    elif opt in ('-c', '--critical'):
        critical = int(arg)

# Verbose mode print
if verbose:
    print 'interface: %s\nDuration: %s\nWarning limit: %s\nCritical limit: %s' % (interface, duration, warning, critical)

# If user is root, update database before check
if getpass.getuser() == 'root':
    os.popen('vnstat -u')
    if verbose:
        print 'User is root. Updating vnstat database.'

# Main program reading output of `vnstat` for specified duration and interface
for line in os.popen('vnstat -i %s --dumpdb' % (interface)).readlines():
    if line.rstrip().startswith('%s;0' % (duration)):

        # Parsing line and computing total sum
        recieved, sended = [int(num) for num in line.split(';')[3:5]]
        total = sended + recieved

        # DEBUG output
        if verbose:
            status('DEBUG', sended, recieved, warning)

        # Comparing total sum with warning and critical limit
        if total >= critical:
            status('CRITICAL', sended, recieved, critical)
            sys.exit(exit['CRITICAL'])
        elif total < critical and total >= warning:
            status('WARNING', sended, recieved, warning)
            sys.exit(exit['WARNING'])
        elif total < warning:
            status('OK', sended, recieved, warning)
            sys.exit(exit['OK'])
        else:
            status('UNKNOWN', sended, recieved, warning)
            sys.exit(exit['UNKNOWN'])
